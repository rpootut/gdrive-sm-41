<?php
error_reporting(0);
 
require __DIR__ . '/vendor/autoload.php';

//crea cliente de google
$client = new Google_Client();
$client->setAuthConfig(__DIR__ . '/service-account-credentials.json');
$client->setScopes(['https://www.googleapis.com/auth/drive']);
$service = new Google_Service_Drive($client);

// Print the names and IDs for up to 10 files.
$optParams = array(
    "'1skGfNJCjTPGzIRuIl7VK0SM9L53Mv7d8' in parents",
    'pageSize' => 10,
    'fields' => 'nextPageToken, files(id, name)'
);

$results = $service->files->listFiles($optParams);

if (count($results->getFiles()) == 0) {
    echo "No files found.\n";
} else {
    echo "Files:\n";
    foreach ($results->getFiles() as $file) {
        echo $file->getName();
        echo ' - ';
        echo $file->getId();
        echo '<br>';
        //printf("%s (%s)\n", $file->getName(), $file->getId());
    }
}
