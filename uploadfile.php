<?php
 
require __DIR__ . '/vendor/autoload.php';

//crea cliente de google
$client = new Google_Client();
$client->setAuthConfig(__DIR__ . '/service-account-credentials.json');
$client->setScopes(['https://www.googleapis.com/auth/drive']);
$service = new Google_Service_Drive($client);

//upload file configuracion
$fileMetadata = new Google_Service_Drive_DriveFile(array(
    'name' => 'tiernito.jpg',
    'parents' => array('1skGfNJCjTPGzIRuIl7VK0SM9L53Mv7d8')//Carpeta padre
));
//obtener informacion del archivo local
$content = file_get_contents('C:\Users\utadmin\Pictures\postman-logo.png');

$file = $service->files->create($fileMetadata, array(
    'data' => $content,
    'mimeType' => 'image/jpeg',
    'uploadType' => 'multipart',
    'fields' => 'id'
));

var_dump($file);